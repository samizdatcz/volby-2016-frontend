window.ig.strany = strany = {}
palettes =
  #CSSD
  7: <[#fea201 #ffb042 #ffbe67 #ffcd89 #ffd9a7 #ffe7c6 #fff2e2]>.reverse!
  #ODS
  53: <[#1c76f0 #5a88f3 #7f9af5 #9caef8 #b7c1fa #d0d6fc #e8eafe]>.reverse!
  #KSČ
  47: <[#f40000 #fd4a2c #ff7252 #ff9376 #ffb199 #ffcbbb #ffe5dd]>.reverse!
  #KDU
  1: <[#fee300 #ffe750 #ffeb74 #fff095 #fff4b2 #fff7cd #fffce8]>.reverse!
  #TOP
  721: <[#b560f3 #c278f5 #ce8ff7 #d9a5f9 #e3bcfb #edd2fd #f6e9fe]>.reverse!
  #SZ
  5: <[#0fb103 #4ebd3a #72c85d #91d47e #afdf9e #caeabe #e4f4de]>.reverse!
  #ANO
  768: <[#eff3ff #c6dbef #9ecae1 #6baed6 #4292c6 #2171b5 #084594]>
window.ig.stranyUid = stranyUid = {}
colors =
  "TOP 09": \#B560F3
  "ANO": \#020046
  "KSČM": \#F40000
  "SZ": \#0FB103
  "ČSSD": \#FEA201
  "ODS": \#1C76F0
  "DSSS": \#B55E01
  "KDU-ČSL": \#FEE300
  "Piráti": \#504E4F
  "ANO 2011": \#020046
for strana in window.ig.data.strany.split "\n" .slice 1
  [id, nazev, uid, zkratka, barva] = strana.split "\t"
  if zkratka
    zkratka .= replace "\r" ""
  if not barva and colors[zkratka]
    barva = colors[zkratka]
  palette = palettes[uid]
  continue unless id
  stranyUid[uid] = strany[id] = {nazev, zkratka, barva, uid, palette}
