idsToNuts = <[CZ042 CZ031 CZ064 CZ041 CZ052 CZ063 CZ051 CZ080 CZ071 CZ053 CZ032 CZ010 CZ020 CZ072]>
class ig.KrajeMap implements ig.utils.supplementalMixin
  (@container, @downloadCache) ->
    ig.Events @
    @element = @container.append \div
      ..attr \class \kraje-map
    @svg = @element.append \svg
    (err, {features, mesh}) <~ @getFeatures!
    @svg
      ..attr \width features.width
      ..attr \height features.height
    areasG = @svg.append \g
      ..attr \class \areas
    @paths = areasG.selectAll \path .data features .enter!append \path
        ..attr \d (.d)
        ..on \click ~> @emit \kraj-click it.properties.nuts
        ..on \touchstart ~> @emit \kraj-click it.properties.nuts
        ..on \mouseover ~> @emit \kraj-mouseover it.properties.nuts
        ..on \mouseout ~> @emit \kraj-mouseout it.properties.nuts
    @validityNotice = @element.append \div
      ..attr \class \validity-notice
    meshG = @svg.append \g
      ..attr \class \mesh
    meshG.append \path .attr \d mesh.d
    @datasource = @downloadCache.getItem "kraje"
    @drawSupplemental!
    (err, data) <~ @datasource.get
    @setData data if data
    @datasource.on \downloaded @~setData

  setStranaId: (@currentStranaId) ->
    @drawResults @currentStranaId

  drawResults: (stranaId) ->
    if stranaId
      @drawStrana stranaId
    else
      @drawWinners!
    @computeSupplemental!
    @updateSupplemental!
    @updateValidityNotice!

  computeSupplemental: ->
    @stats =
      okrsky_celkem : 0
      okrsky_zprac : 0
      zapsani_volici : 0
      platne_hlasy : 0
    for krajId, krajData of @data
      for stat of @stats
        @stats[stat] += krajData.stats[stat]
        @stats[stat] += krajData.stats[stat]
        @stats[stat] += krajData.stats[stat]
        @stats[stat] += krajData.stats[stat]

  updateValidityNotice: ->
    vysledkyType = switch
      | @stats.okrsky_zprac == 0 => "Čekáme na první výsledky"
      | @stats.okrsky_zprac == @stats.okrsky_celkem => "Celkové výsledky"
      | otherwise => "Průběžné výsledky"
    @validityNotice.html "Platné k #{ig.utils.humanDateTime window.ig.liveUpdater.lastMessage} | #{vysledkyType}"

  drawWinners: ->
    krajWinners = {}

    for krajId, krajData of @data
      krajWinners[krajId] = krajData.strany.0
      for strana in krajData.strany
        if krajWinners[krajId].hlasy < strana.hlasy
          krajWinners[krajId] = strana
    @paths.attr \fill ->
      winningPartyId = krajWinners[it.properties.nuts]?id
      if winningPartyId
        window.ig.utils.getStranaColor window.ig.strany[winningPartyId]
      else
        \#eaeaea

  drawStrana: (stranaId) ->
    stranaUid = window.ig.strany[stranaId]?uid
    fieldToUse = "hlasy"
    kraje = for krajId, krajData of @data
      selectedStrana = null
      for strana in krajData.strany
        if stranaUid and stranaUid == window.ig.strany[strana.id]?uid
          selectedStrana = strana
          break
        else if not stranaUid and strana.id == stranaId
          selectedStrana = strana
          break
      if selectedStrana
        selectedStrana.percent = selectedStrana[fieldToUse] / krajData.stats.platne_hlasy
      {krajData, selectedStrana}
    max = d3.max do
      kraje
        .filter (.selectedStrana)
        .map (.selectedStrana.percent)
    palette = if ig.strany[stranaId].palette
      that
    else
      ['#ffffff','#f0f0f0','#d9d9d9','#bdbdbd','#969696','#737373','#525252','#252525']
    scale = d3.scale.linear!
      ..domain ig.utils.divideToParts [0 max], palette.length
      ..range palette
    for kraj in kraje
      color = if kraj.selectedStrana
        scale kraj.selectedStrana.percent
      else
        null
      kraj.krajData.currentKrajColor = color

    @paths.attr \fill ~> @data[it.properties.nuts]?currentKrajColor || \#d9d9d9

  setData: (data) ->
    @data = {}
    for kraj in data
      continue unless kraj.id
      @data[kraj.id] = kraj
    @drawResults @currentStranaId

  getFeatures: (cb) ->
    (err, topo) <~ d3.json "#{ig.addrPrefix}/data/kraje.topo.json"
    bounds =
      [topo.objects.data.bbox[0], topo.objects.data.bbox[1]]
      [topo.objects.data.bbox[2], topo.objects.data.bbox[3]]
    mesh = topojson.mesh topo, topo.objects.data, (a, b) -> a isnt b
    features = topojson.feature topo, topo.objects."data" .features
    @projection = projection = ig.utils.geo.getProjection bounds, {width: 400}
    {width, height} = ig.utils.geo.getDimensions bounds, projection
    features.width = width
    features.height = height
    path = d3.geo.path!
      ..projection projection
    for feature, index in features
      feature.properties.nuts = idsToNuts[index]
      feature.d = path feature
    mesh.d = path mesh
    cb null, {features, mesh}
