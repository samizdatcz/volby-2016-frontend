class ig.GeometryPreparer
  ->
    @cache = {}

  getKraj: (id, cb) ->
    if @cache[id]
      cb null, that
      return
    (err, json) <~ @download id
    return cb err if err
    topo = @process json, id
    cb null, topo

  download: (kraj, cb) ->
    d3.json "https://interaktivni.rozhlas.cz/data-r/volby-2016-frontend/www/data/#{kraj.replace 'CZ' ''}.topo.json", cb

  process: (topo, id) ->
    features = topojson.feature topo, topo.objects."data" .features
    bounds =
      [topo.objects.data.bbox[0], topo.objects.data.bbox[1]]
      [topo.objects.data.bbox[2], topo.objects.data.bbox[3]]
    width = 400
    if id == "CZ071"
      width = 200
    projection = ig.utils.geo.getProjection bounds, {width}
    {width, height} = ig.utils.geo.getDimensions bounds, projection
    features.bounds = bounds
    features.width = width
    features.height = height
    features.projection = projection
    path = d3.geo.path!
      ..projection projection
    for feature in features
      feature.d = path feature
    features

