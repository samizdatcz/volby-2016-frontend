class ig.KrajMap
  (@container, @features) ->
    ig.Events @
    @svg = @container.append \svg
      ..attr \width features.width
      ..attr \height features.height
      ..attr \class \kraj-map
    @featureElements = @svg.selectAll \path .data features .enter!append \path
        ..attr \d (.d)
        ..attr \fill \#eaeaea
        ..on \mouseover ~>
          @emit \obec-mouseover it.properties.ICZUJ, @resultsAssoc[it.properties.ICZUJ]
        ..on \touchstart ~>
          @emit \obec-mouseover it.properties.ICZUJ, @resultsAssoc[it.properties.ICZUJ]
        ..on \mouseout ~>
          @emit \obec-mouseout it.properties.ICZUJ, @resultsAssoc[it.properties.ICZUJ]
    @featuresAssoc = {}
    for feature in @features
      @featuresAssoc[feature.properties.ICZUJ] = feature

  displayStatStrana: (stranaId) ->
    for obec in @results
      continue unless @featuresAssoc[obec.id]
      @featuresAssoc[obec.id].value = obec.stranyAssoc[stranaId] / obec.stats.platne_hlasy
    palette = if ig.strany[stranaId].palette
      that
    else
      ['#ffffff','#f0f0f0','#d9d9d9','#bdbdbd','#969696','#737373','#525252','#252525']
    @displayChoropleth palette

  displayChoropleth: (colors) ->
    scale = d3.scale.quantile!
      ..domain @features.map (.value)
      ..range colors
    @featureElements.attr \fill -> (scale it.value) || \#eaeaea

  displayWinners: ->
    for obec in @results
      continue unless @featuresAssoc[obec.id]
      maxHlasy = 0
      maxStranaId = null
      for stranaId, hlasy of obec.stranyAssoc
        if hlasy > maxHlasy
          maxHlasy = hlasy
          maxStranaId = stranaId
      color = \#eaeaea
      if maxHlasy and maxStranaId and window.ig.strany[maxStranaId]
        color = ig.utils.getStranaColor window.ig.strany[maxStranaId]
      @featuresAssoc[obec.id].value = color
    @featureElements.attr \fill -> it.value || \#eaeaea


  setResults: (@results) ->
    @resultsAssoc = {}
    for obec in @results
      @resultsAssoc[obec.id] = obec


