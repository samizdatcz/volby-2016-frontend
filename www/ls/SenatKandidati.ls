class ig.SenatKandidati
  ->
    @assoc = {}
    @array = []
    for line in d3.tsv.parse ig.data.kandidatiSenat
      id = @getId line.OBVOD, line.CKAND
      line.name = "#{line.JMENO} #{line.PRIJMENI}"
      line.party = line.NAZEV_VS
      line.number = parseInt line.CKAND, 10
      line.district = line.obvod = parseInt line.OBVOD, 10
      line.partyShort = ig.stranyUid[line.VSTRANA]?zkratka
      @assoc[id] = line
      @array.push line

  find: (obvod, kand) ->
    return @assoc[@getId obvod, kand]

  getId: (obvod, kand) ->
    "#obvod-#kand"
